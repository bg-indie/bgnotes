// Добавьте следующий обработчик события для кнопок add-fragment

document.addEventListener('DOMContentLoaded', () => {
  const daysAddFragmentBtn = document.getElementById('days-add-fragment')
  const monthAddFragmentBtn = document.getElementById('month-add-fragment')
  const yearAddFragmentBtn = document.getElementById('year-add-fragment')});

var addButtonDay = document.querySelector('.day-add-fragment');
var addButtonMonth = document.querySelector('.month-add-fragment');
var addButtonYear = document.querySelector('.year-add-fragment');

var toDoListDay = document.querySelector('.day-to-do-list .day-scroll');
var toDoListMonth = document.querySelector('.month-to-do-list .month-scroll');
var toDoListYear = document.querySelector('.year-to-do-list .year-scroll');

var taskId = 0;
var colors = ["#d9d9d9", "#149C22", "#FFB800", "#DD0808"];

addButtonDay.addEventListener('click', function() {
  taskId++;

  var todoFragment = document.createElement('div');
  todoFragment.className = 'todo-fragment';
  todoFragment.id = 'task-' + taskId; 
  
  todoFragment.innerHTML = `
    <input type="checkbox" id="check-${taskId}" class="check" unchecked>
    <label for="check-${taskId}" class="check-label"></label>
    <input type="text" value="do something" class="create-something"></input>
    <button class="remove-fragment" onclick="removeTask(${taskId})">
      <div class="rectangle-4"></div>
      <div class="rectangle-5"></div>
    </button>
    <div class="preority" id="preority-${taskId}" onclick="changePriorityColor(event)"></div>
  `;

  toDoListDay.appendChild(todoFragment);
});

addButtonMonth.addEventListener('click', function() {
  taskId++;

  var todoFragment = document.createElement('div');
  todoFragment.className = 'todo-fragment';
  todoFragment.id = 'task-' + taskId;
  
  todoFragment.innerHTML = `
    <input type="checkbox" id="check-${taskId}" class="check" unchecked>
    <label for="check-${taskId}" class="check-label"></label>
    <input type="text" value="do something" class="create-something"></input>
    <button class="remove-fragment" onclick="removeTask(${taskId})">
      <div class="rectangle-4"></div>
      <div class="rectangle-5"></div>
    </button>
    <div class="preority" id="preority-${taskId}" onclick="changePriorityColor(event)"></div>
  `;

  toDoListMonth.appendChild(todoFragment);
});

addButtonYear.addEventListener('click', function() {
  taskId++;

  var todoFragment = document.createElement('div');
  todoFragment.className = 'todo-fragment';
  todoFragment.id = 'task-' + taskId; 
  
  todoFragment.innerHTML = `
    <input type="checkbox" id="check-${taskId}" class="check" unchecked>
    <label for="check-${taskId}" class="check-label"></label>
    <input type="text" value="do something" class="create-something"></input>
    <button class="remove-fragment" onclick="removeTask(${taskId})">
      <div class="rectangle-4"></div>
      <div class="rectangle-5"></div>
    </button>
    <div class="preority" id="preority-${taskId}" onclick="changePriorityColor(event)"></div>
  `;

  toDoListYear.appendChild(todoFragment);
});

function changePriorityColor(event) {
  var preorityElement = event.target;
  var preorityId = preorityElement.id;
  var taskId = preorityId.split('-')[1];
  var colorIndex = parseInt(preorityElement.dataset.colorIndex) || 0;
  var nextColorIndex = (colorIndex + 1) % colors.length;

  preorityElement.style.backgroundColor = colors[nextColorIndex];
  preorityElement.dataset.colorIndex = nextColorIndex;
}

function removeTask(taskId) {
  var task = document.getElementById('task-' + taskId);
  if (task) {
    task.remove();
  }
}