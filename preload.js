const { contextBridge, ipcRenderer } = require('electron')

// Изолируем IPC в контексте предзагрузки
contextBridge.exposeInMainWorld(
  'api', {
    addTodoFragment: (type) => {
      ipcRenderer.invoke('add-todo-fragment', type)
    }
  }
)
